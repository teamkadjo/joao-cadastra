# -*- coding: utf-8 -*-

import sys
import os
import django
import pprint
import unidecode

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), ".."))
os.environ['DJANGO_SETTINGS_MODULE'] = 'cadastra.settings'
django.setup()

from cadastra.models import Cadastra

class CadastraScapyPipeline(object):
    def process_item(self, item, spider):
        #print ("***********************")
        #pprint.pprint(item)
        if item:
            res = Cadastra.objects.filter(cadastro = item['Cadastro']).all().count()
            if res == 0:
                cdtro = Cadastra()
                cdtro.atividade = item['Atividade']
                #cdtro.bairro = unidecode.unidecode(item['Bairro'])
                cdtro.bairro = item['Bairro']
                cdtro.cidade = item['Cidade']
                cdtro.cadastro = item['Cadastro']
                cdtro.cadastro = item['Cadastro']
                cdtro.classname = item['ClassName']
                cdtro.cpfcnpj = item['CpfCnpj']
                cdtro.dataabertura = item['DataAbertura']
                cdtro.dataencerramento = item['DataEncerramento']
                cdtro.datavalidadealvara = item['DataValidadeAlvara']
                cdtro.endereco = item['Endereco']
                cdtro.nome = item['Nome']
                cdtro.numeroalvara = item['NumeroAlvara']
                cdtro.observacaoalvara = item['ObservacaoAlvara']
                cdtro.tipocadastro = item['TipoCadastro']
                cdtro.tipopessoa = item['TipoPessoa']
                cdtro.save()
                
        print ("*********************")
        return item



#***********************
#{u'Atividade': u'',
 #u'Bairro': u'ZONA 01',
 #u'Cadastro': 1000002,
 #u'Cidade': u'MARINGA-PR',
 #u'ClassName': u'EntitySituacaoCadastral',
 #u'CpfCnpj': u'',
 #u'DataAbertura': u'',
 #u'DataEncerramento': u'',
 #u'DataValidadeAlvara': u'',
 #u'Endereco': u'AV. BRASIL,S/N - \ufffdREA DE DOM\ufffdNIO P\ufffdBLICO MUNICIPAL QUE INTEGRA O SISTEMA VI\ufffdRIO DA AVENIDA BRASIL.',
 #u'Nome': u'MUNICIPIO DE MARINGA',
 #u'NumeroAlvara': u'',
 #u'ObservacaoAlvara': u'',
 #u'Situacao': u'Normal',
 #u'TipoCadastro': 1,
 #u'TipoPessoa': u'J'}
#*********************
