# -*- coding: utf-8 -*-
import scrapy
import json
from scrapy.http import Request
import random 

agents = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.517 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
]

class CadastraSpider(scrapy.Spider):
    name = 'cadastra'
    allowed_domains = ['venus.maringa.pr.gov.br']
    start_urls = ['http://tributario.maringa.pr.gov.br:8081/aisetributosweb/soap/IIAiseServiceTributos']

    def start_requests(self):        
        url = "http://tributario.maringa.pr.gov.br:8081/aisetributosweb/soap/IIAiseServiceTributos"

        payload = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">  <SOAP-ENV:Body SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">    <axis2wrapped:GetSituacaoCadastro xmlns:axis2wrapped=\"urn:uAiseServiceTributosIntf-IIAiseServiceTributos\">      <CadastroGeral xsi:type=\"xs:int\">{0:08d}</CadastroGeral>      <CNPJ_CPF xsi:type=\"xs:string\"></CNPJ_CPF>      <TipoCadastro xsi:type=\"xs:int\">1</TipoCadastro>      <TipoPessoa xsi:type=\"xs:string\"></TipoPessoa>      <AUserKey xsi:type=\"xs:string\">eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzE0NDY4MTgsIm5iZiI6MTUzMTQ0NjgxOCwiZXhwIjoxNTM0MDM4ODE4LCJpc3MiOiJFV1MiLCJ1c3VhcmlvIjoiV0VCIiwiZW50aWRhZGUiOjEsIm5vbWVlbnRpZGFkZSI6IlBSRUZFSVRVUkEgRE8gTVVOSUNJUElPIERFIE1BUklOR0EifQ.0A1vqGWEJrWcfyHljE55juikYQ7Do07reyIgggnztGY</AUserKey>    </axis2wrapped:GetSituacaoCadastro>  </SOAP-ENV:Body></SOAP-ENV:Envelope>"
        seed_number = 10000000 
        proxy_meta = "http://83.149.70.159:13082"
        the_headers = {
            'host': "tributario.maringa.pr.gov.br:8081",
            'origin': "http://venus.maringa.pr.gov.br:9900",
            'x-requested-with': "ShockwaveFlash/30.0.0.134",
            'soapaction': "urn:uAiseServiceTributosIntf-IIAiseServiceTributos#GetSituacaoCadastro",
            'user-agent': random.choice(agents),
            'dmt': "1",
            'content-type': "application/xml",
            'referer': "http://venus.maringa.pr.gov.br:9900/aisetributosweb//AiseTributosWebFlex.swf",
            }
        
        # for j in range(10017797, 99999999):
        for j in range(1865253, 1999999):
            the_headers['user-agents'] = random.choice(agents)
            print ("*"),
            yield Request(url,self.parse,method="POST",body=payload.format(j),headers= the_headers,meta={"progress":j, "proxy": proxy_meta})


    def parse(self,response):
        print ("****************************")
        print ("Progress: {0}".format(response.request.meta['progress']))
        print ("****************************")
        result = response.xpath("//*/text()").extract_first()
        dict_result = json.loads(result)
        if len(dict_result['list']['items']) >0 :
            for itm in dict_result['list']['items']:            
                print ("Added")
                yield itm
        else:
            print ("Nothing")
