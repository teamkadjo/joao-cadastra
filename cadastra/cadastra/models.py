# -*- coding: utf-8 -*-                 
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from . import settings
from django.contrib import admin

class BaseModel(models.Model):

    created_at = models.DateTimeField(auto_now = True)
    updated_at = models.DateTimeField(default = timezone.now)

    class Meta:
        abstract = True
        
        
class Cadastra(BaseModel):
    cadastro = models.CharField(max_length = 255, unique= True,blank=True)
    atividade = models.CharField(max_length = 255,null=True,blank=True)
    observacaoalvara = models.CharField(max_length = 255, null=True,blank=True)
    situacao = models.CharField(max_length = 255, null=True,blank=True)
    nome = models.CharField(max_length = 255, null=True,blank=True)
    bairro = models.CharField(max_length = 255, null=True,blank=True)
    cidade = models.CharField(max_length = 255, null=True,blank=True)
    datavalidadealvara = models.CharField(max_length = 255, null=True,blank=True)
    dataencerramento = models.CharField(max_length = 255, null=True,blank=True)
    cpfcnpj = models.CharField(max_length = 255, null=True,blank=True)
    classname = models.CharField(max_length = 255, null=True,blank=True)
    numeroalvara = models.CharField(max_length = 255, null=True,blank=True)
    endereco = models.CharField(max_length = 255, null=True,blank=True)
    tipopesso = models.CharField(max_length = 255, null=True,blank=True)
    dataabertura = models.CharField(max_length = 255, null=True,blank=True)
    tipocadastro = models.CharField(max_length = 255, null=True,blank=True)
    

    def __unicode__(self):
        return str(self.cadastro)

    class Meta:
        verbose_name = 'Cadastra'
        verbose_name_plural = 'Cadastras'

class CadastraAdmin(admin.ModelAdmin):
    list_display = ('cadastro', 'nome','cidade','bairro','endereco','updated_at')
    search_fields = ('cadastro','nome','bairro','endereco')


