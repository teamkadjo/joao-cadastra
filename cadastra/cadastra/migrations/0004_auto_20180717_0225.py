# Generated by Django 2.0.7 on 2018-07-17 02:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cadastra', '0003_auto_20180717_0204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cadastra',
            name='atividade',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='bairro',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='cidade',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='classname',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='cpfcnpj',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='dataabertura',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='dataencerramento',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='datavalidadealvara',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='endereco',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='nome',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='numeroalvara',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='observacaoalvara',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='situacao',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='tipocadastro',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='cadastra',
            name='tipopesso',
            field=models.CharField(max_length=50),
        ),
    ]
